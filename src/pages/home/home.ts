import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SinglePostPage } from '../single-post/single-post';
import { Post, media} from '../../providers/service/service.module';
import { ServiceProvider } from '../../providers/service/service';
import { Page } from '../../providers/page-service/page-service.module';
import { PageServiceProvider } from '../../providers/page-service/page-service';
import { SinglePlacePage } from '../single-place/single-place';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  list: Post[] = [];
  postDetail: Post;
  featured_media: number;
  pageList: Page[]=[];
  pageDetail: Page;

  constructor(
    public postService: ServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public pageService: PageServiceProvider
  ) 
  
  {
    this.postDetail =this.navParams.get('post');

    this.pageDetail =this.navParams.get('page');


    this.getPosts(5);


    this.getPages(5);
  }

  ngOnInit(): void {
  }


  getPosts(elements?: number) : void {
    this.postService.get().then(
      (a) => {
        this.list = a.slice(0, elements + this.list.length);

        console.log('homePage - getPosts() - this.list',this.list)
      },
      (b) => console.log("Err", b)
    );
  }

  getPages(elements?: number) : void {
    this.pageService.getPage().then(
      (a) => {
        this.pageList = a.slice(0, elements + this.pageList.length);
        console.log('homePage - getPages() - this.pageList',this.pageList)
      },
      (b) => console.log("Err", b)
    );

  }

  onEventoDettaglio(p:Post){
    this.navCtrl.push(SinglePostPage,{ post: p });
  }

  onPaginaDettaglio(p:Page){
    this.navCtrl.push(SinglePlacePage,{ page: p });
  }


}
