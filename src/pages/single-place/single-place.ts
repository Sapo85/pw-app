import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, IonicPageModule } from 'ionic-angular';
import { Page } from '../../providers/page-service/page-service.module';
import { PageServiceProvider } from '../../providers/page-service/page-service';
import { AppModule } from '../../app/app.module'
/**
 * Generated class for the SinglePlacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-single-place',
  templateUrl: 'single-place.html',
})
@NgModule({
  declarations: [
    SinglePlacePage, AppModule
  ],
  imports: [
    IonicPageModule.forChild(SinglePlacePage),
  ],
  entryComponents: [AppModule],

  exports: [ SinglePlacePage ]


})
export class SinglePlacePage {
  pageDetail:Page;
  mediaPic:string;

  constructor(
    public pageService: PageServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams) 
  {
    this.pageDetail =this.navParams.get('page');
    console.log('page',this.pageDetail)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SinglePlacePage');
  }

  media() : String {
    console.log(this.pageDetail.guid.rendered)
    return this.mediaPic = this.pageDetail.guid.rendered.concat('?_embed');
  }

}
