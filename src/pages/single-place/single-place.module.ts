import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SinglePlacePage } from './single-place';

@NgModule({
  declarations: [
    SinglePlacePage,
  ],
  entryComponents: [SinglePlacePage],
  imports: [
    IonicPageModule.forChild(SinglePlacePage),
  ],
  exports: [ SinglePlacePage ]

})
export class SinglePlacePageModule {}
