import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Post, media } from '../../providers/service/service.module';
import { ServiceProvider } from '../../providers/service/service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'page-single-post',
  templateUrl: 'single-post.html',
})
export class SinglePostPage {

  postDetail: Post;
  mediaNum:string;
  mediaDetail:media;
  list: Post[] = [];
  mediaList: media[] = [];



  constructor(
    public postService: ServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient
    ) 
  
  {
    this.postDetail =this.navParams.get('post');

    console.log('post json: ',this.postDetail);
  }

getMedia():Promise<media[]> {

  return this.http.get<media[]>("https://riminidavivere.immaginificio-test.com/wp-json/wp/v2/post/").toPromise();
}

getPostsJson(elements?: number) : void {
  this.getMedia().then(
    (a) => {
      this.mediaList = a.slice(0, elements + this.list.length);

      console.log('homePage - getMedia() - this.mediaLhist',this.list)
    },
    (b) => console.log("Err", b)
  );
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SinglePostPage');

    console.log('getMedia(): ',this.getMedia)

  }

}
