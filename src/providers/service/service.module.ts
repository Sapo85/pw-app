
export interface Post {
  id: number;
  date: string;
  date_gmt: string;
  guid: GuidOrTitle;
  modified: string;
  modified_gmt: string;
  slug: string;
  status: string;
  type: string;
  link: string;
  title: GuidOrTitle;
  content: ContentOrExcerpt;
  excerpt: ContentOrExcerpt;
  author: number;
  featured_media: number;
  comment_status: string;
  ping_status: string;
  sticky: boolean;
  template: string;
  format: string;
  meta?: (null)[] | null;
  categories?: (number)[] | null;
  tags?: (null)[] | null;
  _links: any;
}
export interface GuidOrTitle {
  rendered: string;
}
export interface ContentOrExcerpt {
  rendered: string;
  protected: boolean;
}

export interface CuriesEntity {
  name: string;
  href: string;
  templated: boolean;
}

export interface media {
  id: number;
  date: string;
  date_gmt: string;
  guid: GuidOrTitleOrDescriptionOrCaption;
  modified: string;
  modified_gmt: string;
  slug: string;
  status: string;
  type: string;
  link: string;
  title: GuidOrTitleOrDescriptionOrCaption;
  author: number;
  comment_status: string;
  ping_status: string;
  template: string;
  meta?: (null)[] | null;
  description: GuidOrTitleOrDescriptionOrCaption;
  caption: GuidOrTitleOrDescriptionOrCaption;
  alt_text: string;
  media_type: string;
  mime_type: string;
  media_details: MediaDetails;
  post: number;
  source_url: string;
  _links: Links;
}
export interface GuidOrTitleOrDescriptionOrCaption {
  rendered: string;
}
export interface MediaDetails {
  width: number;
  height: number;
  file: string;
  sizes: Sizes;
  image_meta: ImageMeta;
}
export interface Sizes {
  thumbnail: ThumbnailOrMediumOrMediumLargeOrLargeOrFull;
  medium: ThumbnailOrMediumOrMediumLargeOrLargeOrFull;
  medium_large: ThumbnailOrMediumOrMediumLargeOrLargeOrFull;
  large: ThumbnailOrMediumOrMediumLargeOrLargeOrFull;
  full: ThumbnailOrMediumOrMediumLargeOrLargeOrFull;
}
export interface ThumbnailOrMediumOrMediumLargeOrLargeOrFull {
  file: string;
  width: number;
  height: number;
  mime_type: string;
  source_url: string;
}
export interface ImageMeta {
  aperture: string;
  credit: string;
  camera: string;
  caption: string;
  created_timestamp: string;
  copyright: string;
  focal_length: string;
  iso: string;
  shutter_speed: string;
  title: string;
  orientation: string;
  keywords?: (null)[] | null;
}
export interface Links {
  self?: (SelfEntityOrCollectionEntityOrAboutEntity)[] | null;
  collection?: (SelfEntityOrCollectionEntityOrAboutEntity)[] | null;
  about?: (SelfEntityOrCollectionEntityOrAboutEntity)[] | null;
  author?: (AuthorEntityOrRepliesEntity)[] | null;
  replies?: (AuthorEntityOrRepliesEntity)[] | null;
}
export interface SelfEntityOrCollectionEntityOrAboutEntity {
  href: string;
}
export interface AuthorEntityOrRepliesEntity {
  embeddable: boolean;
  href: string;
}
