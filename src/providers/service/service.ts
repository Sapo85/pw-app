import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post, media } from './service.module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ServiceProvider Provider');
  }


  get():Promise<Post[]> {

    return this.http.get<Post[]>("https://riminidavivere.immaginificio-test.com/wp-json/wp/v2/posts").toPromise();
  }

  // getMedia(id: number): Observable<string> {
  //   return this.wpApiMedia.get(id)
  //     .map(res => res.json())
  //     .map(data => {
  //       return data[ 'source_url' ];
  //     });
  // }


}
