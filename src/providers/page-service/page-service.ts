import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from './page-service.module';


@Injectable()
export class PageServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello PageServiceProvider Provider');
  }
  getPage():Promise<Page[]> {

    return this.http.get<Page[]>("https://riminidavivere.immaginificio-test.com/wp-json/wp/v2/pages").toPromise();
  }
}
